onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_rx_phy/enable
add wave -noupdate /tb_rx_phy/clk100MHz
add wave -noupdate /tb_rx_phy/clk16kHz
add wave -noupdate /tb_rx_phy/data
add wave -noupdate /tb_rx_phy/payload
add wave -noupdate /tb_rx_phy/recv_byte
add wave -noupdate /tb_rx_phy/new_byte
add wave -noupdate /tb_rx_phy/parity_status
add wave -noupdate /tb_rx_phy/recv_status
add wave -noupdate -divider {Inside Module}
add wave -noupdate -radix unsigned /tb_rx_phy/PS2_TX/state
add wave -noupdate -radix unsigned /tb_rx_phy/PS2_TX/state_next
add wave -noupdate -radix decimal /tb_rx_phy/PS2_TX/count_bits
add wave -noupdate -radix decimal /tb_rx_phy/PS2_TX/count_bits_next
add wave -noupdate -radix binary /tb_rx_phy/PS2_TX/shift_reg_rx
add wave -noupdate -radix binary /tb_rx_phy/PS2_TX/parity
add wave -noupdate -radix binary /tb_rx_phy/PS2_TX/ps2_clk_reg
add wave -noupdate -radix binary /tb_rx_phy/PS2_TX/ps2_dat_reg
add wave -noupdate -radix binary /tb_rx_phy/PS2_TX/new_bit
add wave -noupdate -radix binary /tb_rx_phy/PS2_TX/start_condition_ne_dat
add wave -noupdate -radix binary /tb_rx_phy/PS2_TX/start_condition_ne_clk
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 350
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {733425 ns}
