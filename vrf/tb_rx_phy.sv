`include "classes.sv"

module tb_rx_phy();

    timeunit      1ns;
    timeprecision 1ns;

    parameter SYS_CLK = 10ns;
    parameter PS2_CLK = 62.5us;
    parameter PS2_HCLK = PS2_CLK/2.0;
    parameter NUM_TX = 10;

    // Connection regs and wires
    reg clk100MHz, clk16kHz;
    reg enable, data;
    wire [7:0] recv_byte;
    wire new_byte, parity_status, recv_status;

    // The transaction vector
    ps2_transaction test_vector[NUM_TX];

    ps2m_rx_phy PS2_RX
        (.clk100MHz(clk100MHz),
         .enable(enable),
         .ps2_clk(clk16kHz),
         .ps2_data(data),
         .recv_byte(recv_byte),
         .new_byte(new_byte),
         .parity_status(parity_status),
         .recv_status(recv_status));

    // Clock and test vector generation
    initial begin
        clk100MHz = 0;

        foreach(test_vector[i]) begin
            test_vector[i] = new;
            void'(test_vector[i].randomize());
        end

        fork
            forever begin
                #(SYS_CLK/2) clk100MHz = ~clk100MHz;
            end
        join_none
    end

    initial begin
        // Both lines open drain at VDD
        clk16kHz = 1'b1;
        data     = 1'b1;
        enable   = 1'b1;

        // let line be idle for some time
        #(10us);

        for(int i=0; i < NUM_TX; i = i + 1) begin
            send_tx_to_host(test_vector[i]);
            $display("Tx #%d sent!", i);
        end

        #(1us);

        $finish();

    end

    task send_tx_to_host(ps2_transaction tx);

        begin
            // sending the start condition
            data = tx.start_bit;
            #(PS2_HCLK);
            clk16kHz = 0;
            #(PS2_HCLK);

            // sending the payload
            for(int i=0; i < 8; i = i + 1) begin
                clk16kHz = 1;
                data = tx.payload[i];
                #(PS2_HCLK);
                clk16kHz = 0;
                #(PS2_HCLK);
            end

            // sending the parity bit
            clk16kHz = 1;
            data = tx.parity;
            #(PS2_HCLK);
            clk16kHz = 0;
            #(PS2_HCLK);

            // sending the stop condition
            clk16kHz = 1;
            data = tx.stop_bit;
            #(PS2_HCLK);
            clk16kHz = 0;
            #(PS2_HCLK);
            clk16kHz = 1;
            #(PS2_HCLK);
        end

    endtask

endmodule
