module tb_rx_phy ();

    /*
    parameter SYS_CLK = 10;
    parameter PS2_CLK = 62500;
    parameter PS2_HCLK = PS2_CLK/2.0;

    reg clk100Mhz, clk16kHz;
    reg enable, data;

    wire [7:0] recv_byte;
    wire new_byte, parity_status, recv_status;


    ps2m_rx_phy PS2_TX
        (.clk100MHz(clk100MHz),
         .enable(enable),
         .ps2_clk(clk16kHz),
         .ps2_data(data),
         .recv_byte(recv_byte),
         .new_byte(new_byte),
         .parity_status(parity_status),
         .recv_status(recv_status));

    // Clock generation
    initial begin
        clk100Mhz = 0;
        clk16kHz  = 0;

        fork
            forever begin
                #(SYS_CLK/2) clk100MHz = ~clk100MHz;
            end
            /* if we needed another clock
            forever begin
                #(PS2_CLK/2) clk16kHz = ~clk16kHz;
            end
        join_none
    end

    initial begin
        // Both lines open drain at VDD
        clk16kHz = 1'b1;
        data     = 1'b1;
        enable   = 1'b1;

    end

    task send_tx_to_host(bit [7:0] payload);

        begin
            // sending the start condition
            data = 0;
            #(PS2_HCLK);
            clk16kHz = 0;
            #(PS2_HCLK);

            // sending the payload
            for(int i=0; i < 8; i = i + 1) begin
                clk16kHz = 1;
                data = payload[i];
                #(PS2_HCLK);
                clk16kHz = 0;
                #(PS2_HCLK);
            end

            // sending the stop condition
            clk16kHz = 1;
            data = 0;
            #(PS2_HCLK);
            clk16kHz = 0;
            #(PS2_HCLK);
            clk16kHz = 1;
        end

    endtask
    */
endmodule
