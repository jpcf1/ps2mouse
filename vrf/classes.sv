class ps2_transaction;

    rand reg [7:0] payload;
    rand reg start_bit, stop_bit, parity;
    rand bit [1:0] test_type;
    reg recv_status, parity_status;

    constraint ERRORS {
        this.test_type == 2'b00;
        parity    == (this.test_type[0]) ? |payload : |payload + 1'b1;
        start_bit == 1'b0;
        stop_bit  == (this.test_type[1]) ? 1'b0 : 1'b1;
    }

    function new ();//(bit [1:0] test_type);
        // test_type [1:0] == {wrong_stop_bit, wrong_parity}
        //this.test_type = test_type;
    endfunction

    function post_randomize():
        parity_status = ~test_type[0];
        recv_status   = ~test_type[1];
    endfunction

endclass : ps2_transaction
