module ps2m_rx_phy (input clk100MHz,
     input enable,
     input ps2_clk,
     input ps2_data,
     output reg [7:0] recv_byte,
     output reg       new_byte,
     output reg       parity_status,
     output reg       recv_status);

    // State tags
    localparam IDLE          = 3'd0;
    localparam START_BIT     = 3'd1;
    localparam RX_BYTE       = 3'd2;
    localparam PARITY_BIT    = 3'd3;
    localparam STOP_BIT      = 3'd4;
    localparam END           = 3'd5;

    // State regs and counters
    reg [2:0] state, state_next;
    reg [3:0] count_bits, count_bits_next;

    // Internal signals and incoming SR
    reg [10:0] shift_reg_rx;
    reg parity, parity_next;
    reg [1:0] ps2_clk_reg, ps2_dat_reg;
    reg parity_status_ctrl, recv_status_ctrl, new_byte_ctrl;

    // Resample line signals
    always @(posedge clk100MHz) begin
        ps2_clk_reg <= {ps2_clk_reg[0], ps2_clk};
        ps2_dat_reg <= {ps2_dat_reg[0], ps2_data};
    end

    // the input buffer sr
    always @(negedge ps2_clk_reg[1]) begin
        shift_reg_rx <= {ps2_dat_reg[1], shift_reg_rx[10:1]};
    end

    // The output register and signals
    always @(posedge clk100MHz) begin
        if(state == STOP_BIT)
            recv_byte <= shift_reg_rx[10:2];
        else
            recv_byte <= recv_byte;

        parity_status <= parity_status_ctrl;
        recv_status <= recv_status_ctrl;
        new_byte <= new_byte_ctrl;
    end

    // Control signals
    assign start_condition_ne_dat = (ps2_dat_reg[1] ^ ps2_dat_reg[0]) & (ps2_clk_reg[1]) & (ps2_clk_reg[0]);
    assign start_condition_ne_clk = (ps2_clk_reg[1] ^ ps2_clk_reg[0]) & (ps2_clk_reg[1]);

    // The FSM
    always @(posedge clk100MHz) begin
        state <= state_next;
        count_bits <= count_bits_next;
        parity <= parity_next;
    end

    always @(*) begin
        case (state)
            IDLE:
            begin
                if(start_condition_ne_dat)
                    state_next = START_BIT;
                else
                    state_next = IDLE;

            // Calculate Outputs
            count_bits_next = 4'd0;
            parity_next = 1'b0;
            recv_status_ctrl   = recv_status;
            parity_status_ctrl = parity_status;
            new_byte_ctrl = 0;
            end

            START_BIT:
            begin
                if(start_condition_ne_clk)
                    state_next = RX_BYTE;
                else
                    state_next = START_BIT;

                // Calculate Outputs
                count_bits_next = 4'd0;
                parity_next = 1'b0;
                recv_status_ctrl   = recv_status;
                parity_status_ctrl = parity_status;
                new_byte_ctrl = 0;
            end

            RX_BYTE:
            begin
                if(count_bits == 4'd8)
                    state_next = PARITY_BIT;
                else
                    state_next = RX_BYTE;

                // Calculate Outputs
                if(start_condition_ne_clk) begin
                    count_bits_next = count_bits + 4'd1;
                    parity_next = parity ^ shift_reg_rx[10];
                end
                recv_status_ctrl   = recv_status;
                parity_status_ctrl = parity_status;
                new_byte_ctrl = 0;
            end

            PARITY_BIT:
            begin
                if(start_condition_ne_clk)
                    state_next = STOP_BIT;
                else
                    state_next = PARITY_BIT;

                // Calculate Outputs
                count_bits_next = 4'd0;
                recv_status_ctrl   = recv_status;
                parity_status_ctrl = parity;
                new_byte_ctrl = 0;
            end

            STOP_BIT:
            begin
                if(start_condition_ne_clk)
                    state_next = END;
                else
                    state_next = STOP_BIT;

                // Calculate Outputs
                count_bits_next = 4'd0;
                recv_status_ctrl   = ps2_dat_reg[1];
                parity_status_ctrl = parity_status;
                new_byte_ctrl = 0;
            end

            END:
            begin
                state_next = IDLE;

                // Calculate Outputs
                count_bits_next    = 4'd0;
                recv_status_ctrl   = recv_status;
                parity_status_ctrl = parity_status;
                new_byte_ctrl = 1;
            end

            default:
            begin
                state_next = IDLE;
                // Calculate Outputs
                count_bits_next = 4'd0;
                recv_status_ctrl   = recv_status;
                parity_status_ctrl = parity_status;
                new_byte = 0;
            end

        endcase
    end

endmodule
